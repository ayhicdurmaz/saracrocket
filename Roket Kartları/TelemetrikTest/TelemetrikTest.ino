//Defining Libraries
#include "SoftwareSerial.h"
#include "MPU6050_tockn.h"
#include "Seeed_BME280.h"
#include "TinyGPS++.h"
#include "Kalman.h"
#include "Servo.h"
#include "Wire.h"
#include "SPI.h"
#include "SD.h"

//States
#define LoraConnection //comment if you use lora

//Configration
#define BaudRate 115200

//Constanst
#define MZ A14
#define MO A13
#define Buzzer 33
#define voltmeter A15

//BME280 variables
BME280 bme;
float baseline, pressure;
float alt, apogeeAlt, bottomAlt;
float verticalVelocity, curAlt;

//MPU6050 variables
MPU6050 mpu(Wire);
float g;
float x_offset, y_offset;
double kalAngleX, kalAngleY; // Calculated angle using a Kalman filter
double gyroXangle, gyroYangle; // Angle calculate using the gyro only

//GPS
TinyGPSPlus gps;
byte satVal;
double latitude;
double longtitude;
unsigned long distanceToBase;
double firstLat, firstLong;
String strlatitude = "0.0000000";
String strlongtitude = "0.0000000";

// Create the Kalman instances
Kalman kalmanX;
Kalman kalmanY;

//Sistem variables
float volt       = 0;
int state        = 0;
uint32_t dtime   = 0;
uint32_t timer   = 0;
float loraTimer  = 0;
int fallingCheck = 0;
float firstTime  = 0;
String data      = "";

void setup() {

  delay(100);  //Sensörlere güç gitmesi ve i2c için beklenmesi için bekleme

  Serial.begin(BaudRate);

  Serial1.begin(BaudRate);  //lora haberleşmesi başlatma

  Serial2.begin(BaudRate);  //GPS başlatılması

  pinMode(voltmeter, INPUT);

  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);
  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  Wire.begin(); // I2C

  mpu.begin();  //Accellometre ve Gyroskobun açılması

  if (!bme.init()) {
    Serial.println("Device error!");
  }

  baseline = bme.getPressure();

  pinMode(Buzzer, OUTPUT);

  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}

void loop() {
  mpu.update();  //MPU sensor update
  readGPS();
  readVolt();

  pressure = bme.getPressure(); //hPa türüne çevrildi
  alt = bme.calcAltitude(pressure, baseline);

  calcV(alt, curAlt);
  curAlt = alt;

  if (alt > apogeeAlt) {
    apogeeAlt = alt;
  }

  //Açı ölçme
  kalmanFiltering();

  // G kuvveti hesaplama
  g = calcG(mpu.getAccX(), mpu.getAccY(), mpu.getAccZ());

  if (millis() - loraTimer > 100) {
    sendData();
    loraTimer = millis();
  }
}

void readVolt() {
  volt = float(analogRead(voltmeter)) * 10 / 1023;
}

void readGPS() {
  while (Serial2.available() > 1)
  {
    gps.encode(Serial2.read());
    satVal = gps.satellites.value();
    if (firstTime >= 10) {
      if (gps.location.isUpdated())
      {
        firstLat = gps.location.lat();
        firstLong = gps.location.lng();
        firstTime++;
      }
    }
    else if (gps.location.isUpdated())
    {
      latitude = gps.location.lat();
      longtitude = gps.location.lng();
      strlatitude = String(latitude, 6);
      strlongtitude = String(longtitude, 6);
      distanceToBase =
        (unsigned long)TinyGPSPlus::distanceBetween(
          latitude,
          longtitude,
          firstLat,
          firstLong) / 1000;
    }
  }
}

void calcV(float x1, float x2) {
  double dt = (double)(millis() - dtime) / 100; // Calculate delta time
  dtime = millis();
  verticalVelocity = (x1 - x2) / dt;
}

float calcG(float accX, float accY, float accZ) {
  return (sqrt(pow(accX, 2) + pow(accY, 2) + pow(accZ, 2))); //G kuvvetinin hesaplanması
}

void kalmanFiltering() {
  float accX = mpu.getRawAccX();
  float accY = mpu.getRawAccY();
  float accZ = mpu.getRawAccZ();
  float tempRaw = mpu.getRawTemp();
  float gyroX = mpu.getRawGyroX();
  float gyroY = mpu.getRawGyroY();
  float gyroZ = mpu.getRawGyroZ();

  double dt = (double)(micros() - timer) / 1000000; // Calculate delta time
  timer = micros();

  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;

  double gyroXrate = gyroX / 131.0; // Convert to deg/s
  double gyroYrate = gyroY / 131.0; // Convert to deg/s

  // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
  if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
    kalmanX.setAngle(roll);
    kalAngleX = roll;
    gyroXangle = roll;
  } else
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter

  if (abs(kalAngleX) > 90)
    gyroYrate = -gyroYrate; // Invert rate, so it fits the restriced accelerometer reading
  kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);

  gyroXangle += gyroXrate * dt; // Calculate gyro angle without any filter
  gyroYangle += gyroYrate * dt;

  // Reset the gyro angle when it has drifted too much
  if (gyroXangle < -180 || gyroXangle > 180)
    gyroXangle = kalAngleX;
  if (gyroYangle < -180 || gyroYangle > 180)
    gyroYangle = kalAngleY;
}

void setData() {
  data =  "TG";                                // 0. data type
  data += "?";  data += int(millis() / 1000);   // 1. System time ///
  data += "?";  data += volt;                  // 2. System Volt //
  data += "?";  data += alt;                   // 3. latest alt //
  data += "?";  data += apogeeAlt;             // 4. apogee ///
  data += "?";  data += verticalVelocity;      // 5. vertical velocity //
  data += "?";  data += pressure / 100;              // 6. pressure (hPa) //
  data += "?";  data += g;                     // 7. g power  //
  data += "?";  data += strlatitude;           // 8. Latitude
  data += "?";  data += strlongtitude;         // 9. Longtitude
  data += "?";  data += satVal;                // 10. Satellite Value ///
  data += "?";  data += kalAngleX;             // 11. kalman X angle //
  data += "?";  data += kalAngleY;             // 12. kalman Y angle //
  data += "?";  data += distanceToBase;        // 13. Distance To Base ///
  data += "?";  data += state;                 // 14. state
  data += "?";  data += "*";                   // 15. finish command
}

void sendData() {
  setData();
#ifdef LoraConnection
  Serial1.println(data);
  Serial1.flush();   // buffer temizlemek
#else
  Serial.println(data);
  Serial.flush();   // buffer temizlemek
#endif
}
