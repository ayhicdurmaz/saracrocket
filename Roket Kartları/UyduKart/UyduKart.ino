//Defining Libraries
#include "TinyGPS++.h"
#include "SPI.h"
#include "SD.h"

//States
#define LoraConnection //comment if you use lora

#define LoraTimerHz 2
#define AltWhenFlying 0 //Altitude value for understand that rocket flies
#define BaudRate 115200 //BaudRate value for each module

//Constanst
//Lora
#define MZ A13 //Lora M1
#define MO A14 //Lora M0

//Internals
#define Red 31  //rgb R
#define Blue 30  //rgb B
#define Green 32  //rgb G
#define Led 13    //internal Led
#define Buzzer 33
#define Button 7
#define voltmeter A15
#define ChipSelect 11

//GPS
TinyGPSPlus gps;
byte satVal;
float latitude;
float longtitude;
String strlatitude = "0";
String strlongtitude = "0";

//Sistem variables
float volt       = 0;
float loraTimer  = 0;
String data      = "";

void setup() {

  delay(100);  //Sensörlere güç gitmesi ve i2c için beklenmesi için bekleme

  //Begins
  Serial.begin(BaudRate);   //Serial haberleşme başlatılması
  Serial1.begin(BaudRate);  //lora haberleşmesi başlatma
  Serial2.begin(BaudRate);  //GPS başlatılması
  if (!SD.begin(ChipSelect)) {
    BuzzerError();
  }

  //Pin Sets
  pinMode(voltmeter, INPUT);
  pinMode(Button, INPUT);
  pinMode(Buzzer, OUTPUT);
  pinMode(Red, OUTPUT);
  pinMode(Green, OUTPUT);
  pinMode(Blue, OUTPUT);
  pinMode(Led, OUTPUT);
  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);

  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  setData();
  dataLogger();
  BuzzerOpening();
  RGB(255, 0, 0);
  delay(1000);
  RGB(0, 0, 0);
}

void loop() {
  readGPS();
  readVolt();

  batteryLevel();

  if (millis() - loraTimer > (1000 / LoraTimerHz)) {
    sendData();
    loraTimer = millis();
  }
  else {
    setData();
    dataLogger();
  }
}


void readVolt() {
  volt = float(analogRead(voltmeter)) * 10 / 1023;
}

void readGPS() {
  while (Serial2.available() > 1)
  {
    gps.encode(Serial2.read());
    satVal = gps.satellites.value();
    if (gps.location.isUpdated())
    {
      latitude = gps.location.lat();
      longtitude = gps.location.lng();
      strlatitude = String(latitude, 6);
      strlongtitude = String(longtitude, 6);
    }
  }
}

void setData() {
  data =  "U";                                 // 0. data type
  data += "?";  data += int(millis() / 1000);  // 1. System time
  data += "?";  data += volt;                  // 2. System Volt
  data += "?";  data += strlatitude;           // 3. Latitude
  data += "?";  data += strlongtitude;         // 4. Longtitude
  data += "?";  data += satVal;                // 5. Satellite Value
  data += "?";  data += "*";                   // 6. finish command
}

void sendData() {
  setData();
#ifdef LoraConnection
  Serial1.println(data);
  Serial1.flush();   // buffer temizlemek
#else
  Serial.println(data);
  Serial.flush();   // buffer temizlemek
#endif
}

void BuzzerOpening() {
  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}

void BuzzerError() {
  tone(Buzzer, 1600);
  delay(100);
  tone(Buzzer, 1400);
  delay(100);
  tone(Buzzer, 1600);
  delay(100);
  tone(Buzzer, 1400);
  delay(100);
  noTone(Buzzer);
}

void RGB(int red, int green, int blue) {
  analogWrite(Red, red);
  analogWrite(Green, green);
  analogWrite(Blue, blue);
}

void batteryLevel() {
  while (digitalRead(Button) == HIGH) {
    int batLev = map(analogRead(A0), 720, 1023, 0, 255);
    int y = batLev;
    int r = 255 - batLev;
    RGB(r, y, 0);
  }
  RGB(0, 0, 0);
}

void dataLogger() {
  File dataFile = SD.open("log.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.println(data);
    dataFile.close();
  }
}
