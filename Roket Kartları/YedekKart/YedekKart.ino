//Defining Libraries
#include "MovingAverage.h"
#include "MPU6050_tockn.h"
#include "SFE_BMP180.h"
#include "TinyGPS++.h"
#include "Kalman.h"
#include "Servo.h"
#include "Wire.h"
#include "SPI.h"
#include "SD.h"

//States
#define LoraConnection //comment if you use lora

//Configration
#define UpperServoRate 10 //Açılış derecesi
#define LowerServoRate 10 //Açılış derecesi
#define LoraTimerHz 5
#define AltWhenFlying 0 //Altitude value for understand that rocket flies
#define BaudRate 115200 //BaudRate value for each module
#define FallCounter 15 //Dam value for counter
#define DamValue -50 //Dam value for running algorithms 
#define FallDam 1    //Dam value for counter

//Constanst
//Lora
#define MZ A11 //Lora M1
#define MO A12 //Lora M0

//Internals
#define Red 7  //rgb R
#define Blue 6  //rgb B
#define Green 8  //rgb G
#define Led 13 //internal Led
#define Buzzer 33
#define Button 31
#define voltmeter A0
#define ChipSelect 11

//MultiPlexers
#define UpperMP 47   // MultiPlexer
#define LowerMP 45   // MultiPlexer

//Servos
#define UpperServo 46  //Mechanic system servo1
#define LowerServo 44  //Mechanic system servo2

//BMP180 variables
SFE_BMP180 bmp;
float baseline, pressure;
float alt, apogeeAlt, bottomAlt;
MovingAverage <float> test(20);

//MPU6050 variables
MPU6050 mpu(Wire);
float g;
float x_offset, y_offset;
double kalAngleX, kalAngleY; // Calculated angle using a Kalman filter

//GPS
TinyGPSPlus gps;
byte satVal;
float latitude;
float longtitude;
String strlatitude = "0.0000000";
String strlongtitude = "0.0000000";

// Create the Kalman instances
Kalman kalmanX;
Kalman kalmanY;

//Servo variable
Servo upperServo;
Servo lowerServo;

//Sistem variables
float volt       = 0;
uint32_t timer   = 0;
float loraTimer  = 0;
int state        = 0;
int fallingCheck = 0;
String data      = "";
bool wrongG      = false;
bool apogee      = false;

void setup() {

  delay(100);  //Sensörlere güç gitmesi ve i2c için beklenmesi için bekleme

  //Begins
  Serial.begin(BaudRate);   //Serial haberleşme başlatılması

  Serial1.begin(BaudRate);  //lora haberleşmesi başlatma

  Serial2.begin(BaudRate);  //GPS başlatılması

  Serial3.begin(BaudRate);  //Anakartla bağlantı

  Wire.begin(); // I2C

  mpu.begin();  //Accellometre ve Gyroskobun açılması

  if (!SD.begin(ChipSelect)) {
    BuzzerError();
  }

  if (!bmp.begin()) {
    BuzzerError();
  }

  //Pin Sets
  pinMode(voltmeter, INPUT);
  pinMode(Button, INPUT);

  pinMode(UpperMP, OUTPUT); //MultiPlexer
  pinMode(LowerMP, OUTPUT); //MultiPlexer
  pinMode(Buzzer, OUTPUT);
  pinMode(Green, OUTPUT);
  pinMode(Blue, OUTPUT);
  pinMode(Red, OUTPUT);
  pinMode(Led, OUTPUT);
  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);

  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  upperServo.attach(UpperServo);
  lowerServo.attach(LowerServo);

  calcOffset(); //Offset values for mpu

  setData();
  dataLogger();
  BuzzerOpening();
  RGB(255, 0, 0);
  delay(1000);
  RGB(0, 0, 0);

  state = 1; // Sistem Çalıştı
}

void loop() {
  mpu.update();  //MPU sensor update
  readGPS();
  readVolt();

  batteryLevel();

  if (!wrongG) {
    if (Serial3.available() > 1) {
      char incomingData = Serial3.read();
      if (incomingData == '+')
        Serial3.println('|');
      Serial3.flush();
      wrongG = true;
      tone(Buzzer, 1000);
      delay(250);
      noTone(Buzzer);
    }
  }

  pressure = getPressure(); //hPa türüne çevrildi
  pressure = test.CalculateMovingAverage(pressure);
  alt = bmp.altitude(pressure, baseline);
  g = calcG(mpu.getAccX(), mpu.getAccY(), mpu.getAccZ());

  if (alt > apogeeAlt) {
    apogeeAlt = alt;
    bottomAlt = alt - FallDam;
  }

  //Açı ölçme
  kalmanFiltering();

  checkForApogee();

  if (apogee == false) {
    if (state == 1) { //Roket uçmaya başladı
      if (alt > AltWhenFlying) {
        state = 2;
      }
    }

    if (g < 0.2 && state == 2) {
      state = 3;
    }
  }
  
  if (wrongG == true) {
    if (apogee) {
      if (state < 4) {
        upperServoControl(0);
        state = 4;
      }
      if (alt < 850) {
        lowerServoControl(0);
        state = 5;
      }
      
      if (state == 5) {
        if (alt < 150) {
          state = 6;
        }
      }
    }

    if (millis() - loraTimer > 1000 / LoraTimerHz) {
      sendData();
      loraTimer = millis();
    }
  } else if (wrongG == false) {
    setData();
    dataLogger();
  }
}

void checkForApogee() {
  if (alt > DamValue) {
    if (abs(kalAngleX) >= 65) {
      apogee = true;
      tone(Buzzer, 1000);
      delay(100);
      noTone(Buzzer);
    }
    else if (abs(kalAngleY) >= 65) {
      apogee = true;
      tone(Buzzer, 1000);
      delay(100);
      noTone(Buzzer);
    }
    if (bottomAlt > alt) {
      bottomAlt = alt;
      fallingCheck++;
    }
    else if (alt > bottomAlt) {
      if (fallingCheck > 0) {
        fallingCheck = 0;
      }
    }
    if (fallingCheck > FallCounter) {
      apogee = true;
      upperServoControl(UpperServoRate);
      tone(Buzzer, 2000);
      delay(250);
      noTone(Buzzer);
    }
  }
}

void upperServoControl(int rate) {
  digitalWrite(UpperMP, HIGH);
  upperServo.attach(rate);
}

void lowerServoControl(int rate) {
  digitalWrite(LowerMP, HIGH);
  lowerServo.attach(rate);
}

void readVolt() {
  volt = float(analogRead(voltmeter)) * 10 / 1023;
}

void readGPS() {
  while (Serial2.available() > 1)
  {
    gps.encode(Serial2.read());
    satVal = gps.satellites.value();
    if (gps.location.isUpdated())
    {
      latitude = gps.location.lat();
      longtitude = gps.location.lng();
      strlatitude = String(latitude, 6);
      strlongtitude = String(longtitude, 6);
    }
  }
}

float calcG(float accX, float accY, float accZ) {
  return (sqrt(pow(accX, 2) + pow(accY, 2) + pow(accZ, 2))); //G kuvvetinin hesaplanması
}

void calcOffset() {
  for (int i = 0; i < 1000; i++) { //Offset değerlerini ayarlama
    mpu.update();
    kalmanFiltering();
    x_offset += kalAngleX;
    y_offset += kalAngleY;
  }

  baseline = getPressure();  //hPa türünde

  x_offset = x_offset / 1000;
  y_offset = y_offset / 1000;
}

void kalmanFiltering() {
  float accX = mpu.getRawAccX();
  float accY = mpu.getRawAccY();
  float accZ = mpu.getRawAccZ();
  float tempRaw = mpu.getRawTemp();
  float gyroX = mpu.getRawGyroX();
  float gyroY = mpu.getRawGyroY();
  float gyroZ = mpu.getRawGyroZ();

  double dt = (double)(micros() - timer) / 1000000; // Calculate delta time
  timer = micros();

  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;

  double gyroXrate = gyroX / 131.0; // Convert to deg/s
  double gyroYrate = gyroY / 131.0; // Convert to deg/s

  // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
  if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
    kalmanX.setAngle(roll);
    kalAngleX = roll;
  } else
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter

  if (abs(kalAngleX) > 90)
    gyroYrate = -gyroYrate; // Invert rate, so it fits the restriced accelerometer reading
  kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);
}

void setData() {
  data =  "Y";                                 // 0. data type
  data += "?";  data += (millis() / 1000);     // 1. System time
  data += "?";  data += volt;                  // 2. System Volt
  data += "?";  data += alt;                   // 3. latest alt
  data += "?";  data += apogeeAlt;             // 4. apogee
  data += "?";  data += pressure;              // 5. pressure (hPa)
  data += "?";  data += g;                     // 6. g power
  data += "?";  data += strlatitude;           // 7. Latitude
  data += "?";  data += strlongtitude;         // 8. Longtitude
  data += "?";  data += satVal;                // 9. Satellite Value
  data += "?";  data += kalAngleX;             // 10. kalman X angle
  data += "?";  data += kalAngleY;             // 11. kalman Y angle
  data += "?";  data += state;                 // 12. state
  data += "?";  data += "*";                   // 13. finish command
}

void sendData() {
  setData();
#ifdef LoraConnection
  Serial1.println(data);
  Serial1.flush();   // buffer temizlemek
#else
  Serial.println(data);
  Serial.flush();   // buffer temizlemek
#endif
}

void BuzzerOpening() {
  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}

void BuzzerError() {
  tone(Buzzer, 1600);
  delay(100);
  tone(Buzzer, 1400);
  delay(100);
  tone(Buzzer, 1600);
  delay(100);
  tone(Buzzer, 1400);
  delay(100);
  noTone(Buzzer);
}

void RGB(int red, int green, int blue) { // Kullanılacak
  analogWrite(Red, red);
  analogWrite(Green, green);
  analogWrite(Blue, blue);
}

void batteryLevel() {
  while (digitalRead(Button) == HIGH) {
    int batLev = map(analogRead(A0), 720, 1023, 0, 255);
    int y = batLev;
    int r = 255 - batLev;
    RGB(r, y, 0);
  }
  RGB(0, 0, 0);
}

void dataLogger() {
  File dataFile = SD.open("log.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.println(data);
    dataFile.close();
  }
}

double getPressure() {
  char status;
  double T, P, p0, a;
  status = bmp.startTemperature();
  if (status != 0)
  {
    delay(status);
    status = bmp.getTemperature(T);
    if (status != 0)
    {
      status = bmp.startPressure(3);
      if (status != 0)
      {
        delay(status);
        status = bmp.getPressure(P, T);
        if (status != 0)
        {
          return (P);
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");
}
