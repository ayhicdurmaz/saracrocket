//Defining Libraries
#include "MovingAverage.h"
#include "MPU6050_tockn.h"
#include "Seeed_BME280.h"
#include "TinyGPS++.h"
#include "Kalman.h"
#include "Servo.h"
#include "Wire.h"
#include "SPI.h"
#include "SD.h"

//States
#define LoraConnection //comment if you use lora

//Configration
#define UpperServoRate 10 //Açılış derecesi
#define LowerServoRate 10 //Açılış derecesi
#define LoraTimerHz 5
#define AltWhenFlying 0 //Altitude value for understand that rocket flies
#define BaudRate 115200 //BaudRate value for each module
#define FallCounter 15 //Dam value for counter
#define DamValue -50 //Dam value for running algorithms 
#define FallDam 1    //Dam value for recognize falling

//Constanst
//Lora
#define MZ A13 //Lora M1
#define MO A14 //Lora M0

//Internals
#define Red 31  //rgb R
#define Blue 30  //rgb B
#define Green 32  //rgb G
#define Led 13    //internal Led
#define Buzzer 33
#define Button 7
#define voltmeter A15
#define ChipSelect 11

//MultiPlexers
#define UpperMP 47   // MultiPlexer
#define LowerMP 45   // MultiPlexer

//Servos
#define UpperServo 46  //Mechanic system servo1
#define LowerServo 44  //Mechanic system servo2

//BME280 variables
BME280 bme;
float baseline, pressure;
float alt, apogeeAlt, bottomAlt;
MovingAverage <float> test(20);

//MPU6050 variables
MPU6050 mpu(Wire);
float g;
float x_offset, y_offset;
double gyroXangle, gyroYangle; // Angle calculate using the gyro only

//GPS
TinyGPSPlus gps;
byte satVal;
float latitude;
float longtitude;
String strlatitude = "0";
String strlongtitude = "0";

// Create the Kalman instances
Kalman kalmanX;
Kalman kalmanY;

//Servo variable
Servo upperServo;
Servo lowerServo;

//Sistem variables
float volt       = 0;
float loraTimer  = 0;
int fallingCheck = 0;
int errorCounter = 0;
int state        = 0;
String data      = "";
bool apogee      = false;
bool error       = false;

void setup() {

  delay(100);  //Sensörlere güç gitmesi ve i2c için beklenmesi için bekleme

  //Begins
  Serial.begin(BaudRate);   //Serial haberleşme başlatılması
  Serial1.begin(BaudRate);  //lora haberleşmesi başlatma
  Serial2.begin(BaudRate);  //GPS başlatılması
  Serial3.begin(BaudRate);  // YedekKart bağlantısı
  Wire.begin(); // I2C
  mpu.begin();  //Accellometre ve Gyroskobun açılması
  if (!SD.begin(ChipSelect)) {
    BuzzerError();
  }
  if (!bme.init()) {
    BuzzerError();
    error = true;
  }

  //Pin Sets
  pinMode(voltmeter, INPUT);
  pinMode(Button, INPUT);
  pinMode(Buzzer, OUTPUT);
  pinMode(Red, OUTPUT);
  pinMode(Green, OUTPUT);
  pinMode(Blue, OUTPUT);
  pinMode(Led, OUTPUT);
  pinMode(UpperMP, OUTPUT); //MultiPlexer
  pinMode(LowerMP, OUTPUT); //MultiPlexer
  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);

  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  upperServo.attach(UpperServo);
  lowerServo.attach(LowerServo);

  //Set servo to defaults

  for (int i = 0; i < 100; i++ ) {
    baseline = bme.getPressure();
  }

  if (!error) {
    setData();
    dataLogger();
    BuzzerOpening();
    RGB(255, 0, 0);
    delay(1000);
    RGB(0, 0, 0);

    state = 1;
  }

}

void loop() {
  mpu.update();  //MPU sensor update
  readGPS();
  readVolt();

  batteryLevel();

  pressure = bme.getPressure(); //hPa türüne çevrildi
  pressure = test.CalculateMovingAverage(pressure);
  alt = bme.calcAltitude(pressure, baseline);
  gyroXangle = mpu.getAngleX();
  gyroYangle = mpu.getAngleY();
  g = calcG(mpu.getAccX(), mpu.getAccY(), mpu.getAccZ());

  if (alt > apogeeAlt) {
    apogeeAlt = alt;
    bottomAlt = alt - FallDam;
  }

  checkForApogee();

  if (apogee == false) {
    if (state == 1) { //Roket uçmaya başladı
      if (alt > AltWhenFlying) {
        state = 2;
      }
    }
    if (g < 0.2 && state == 2) {
      state = 3;
    }
    if (!error) {
      if (alt < -100 || alt > 10000 || g < -150 || g > 150) {
        if (errorCounter > 100) {
          BuzzerError();
          Serial3.println('?');
          Serial3.flush();
        }
        errorCounter++;
      } else {
        errorCounter = 0;
      }
    }
    if (Serial3.available() > 0 ) {
      char a = Serial3.read();
      if (a == '|') {
        error = true;
      }
    }
  }
  
  if (!error) {
    if (apogee == true) {
      if (state < 5) {
        //Test edilecek servoların açılması burdamı yoksa fonksiyonda mı
        state = 5;
      }
      if (alt < 600) {
        //servo tetiklemesi
        state = 6;
      }
    }
    if (state == 6) {
      if (alt < 150) {
        state = 7;
      }
    }

    if (millis() - loraTimer > (1000 / LoraTimerHz)) {
      sendData();
      loraTimer = millis();
    }
  } else {
    setData();
    dataLogger();
  }
}

void checkForApogee() {
  if (alt > DamValue) {
    if (g < 0.2) {
      apogee = true;
      upperServoControl(UpperServoRate);
      tone(Buzzer, 1000);
      delay(100);
      noTone(Buzzer);
    }
    if (bottomAlt > alt) {
      bottomAlt = alt;
      fallingCheck++;
    }
    else if (alt > bottomAlt) {
      if (fallingCheck > 0) {
        fallingCheck = 0;
      }
    }
    if (fallingCheck > FallCounter) {
      apogee = true;
      upperServoControl(UpperServoRate);
      tone(Buzzer, 2000);
      delay(250);
      noTone(Buzzer);
    }
  }
}

void upperServoControl(int rate) {
  digitalWrite(UpperMP, HIGH);
  upperServo.attach(rate);
  digitalWrite(UpperMP, LOW);
}

void lowerServoControl(int rate) {
  digitalWrite(LowerMP, HIGH);
  lowerServo.attach(rate);
  digitalWrite(LowerMP, LOW);
}

void readVolt() {
  Serial.println(analogRead(voltmeter));
  volt = float(analogRead(voltmeter)) * 10 / 1023;
}

void readGPS() {
  while (Serial2.available() > 1)
  {
    gps.encode(Serial2.read());
    satVal = gps.satellites.value();
    if (gps.location.isUpdated())
    {
      latitude = gps.location.lat();
      longtitude = gps.location.lng();
      strlatitude = String(latitude, 6);
      strlongtitude = String(longtitude, 6);
    }
  }
}

float calcG(float accX, float accY, float accZ) {
  return (sqrt(pow(accX, 2) + pow(accY, 2) + pow(accZ, 2))); //G kuvvetinin hesaplanması
}

void setData() {
  data =  "A";                                 // 0. data type
  data += "?";  data += int(millis() / 1000);  // 1. System time
  data += "?";  data += volt;                  // 2. System Volt
  data += "?";  data += alt;                   // 3. latest alt
  data += "?";  data += apogeeAlt;             // 4. apogee
  data += "?";  data += pressure / 100;        // 5. pressure (hPa)
  data += "?";  data += g;                     // 6. g power
  data += "?";  data += strlatitude;           // 7. Latitude
  data += "?";  data += strlongtitude;         // 8. Longtitude
  data += "?";  data += satVal;                // 9. Satellite Value
  data += "?";  data += gyroXangle;            // 10. kalman X angle
  data += "?";  data += gyroYangle;            // 11. kalman Y angle
  data += "?";  data += state;            // 12. State
  data += "?";  data += "*";                   // 13. finish command
}

void sendData() {
  setData();
#ifdef LoraConnection
  Serial1.println(data);
  Serial1.flush();   // buffer temizlemek
#else
  Serial.println(data);
  Serial.flush();   // buffer temizlemek
#endif
}

void BuzzerOpening() {
  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}

void BuzzerError() {
  tone(Buzzer, 1600);
  delay(100);
  tone(Buzzer, 1400);
  delay(100);
  tone(Buzzer, 1600);
  delay(100);
  tone(Buzzer, 1400);
  delay(100);
  noTone(Buzzer);
}

void RGB(int red, int green, int blue) {
  analogWrite(Red, red);
  analogWrite(Green, green);
  analogWrite(Blue, blue);
}

void batteryLevel() {
  while (digitalRead(Button) == HIGH) {
    int batLev = map(analogRead(A0), 720, 1023, 0, 255);
    int y = batLev;
    int r = 255 - batLev;
    RGB(r, y, 0);
  }
  RGB(0, 0, 0);
}

void dataLogger() {
  File dataFile = SD.open("log.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.println(data);
    dataFile.close();
  }
}
