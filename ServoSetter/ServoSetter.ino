#include <Servo.h>

#define MZ A11 //Lora M1
#define MO A12 //Lora M0
#define BaudRate 115200 //BaudRate value for each module

//Internals
#define Red 7  //rgb R
#define Blue 6  //rgb B
#define Green 8  //rgb G
#define Led 13    //internal Led
#define Buzzer 33
#define Button 31
#define voltmeter A0
#define ChipSelect 11

//MultiPlexers
#define UpperMP 47   // MultiPlexer
#define LowerMP 45   // MultiPlexer

//Servos
#define UpperServo 46  //Mechanic system servo1
#define LowerServo 44  //Mechanic system servo2

//Servo variable
Servo upperServo;
Servo lowerServo;

int charCounter = 0;
bool upper = false;
bool lower = false;
int data = 0;

void setup() {
  //Begins
  Serial.begin(BaudRate);   //Serial haberleşme başlatılması

  Serial1.begin(BaudRate);  //lora haberleşmesi başlatma

  pinMode(UpperMP, OUTPUT); //MultiPlexer
  pinMode(LowerMP, OUTPUT); //MultiPlexer
  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);

  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  upperServo.attach(46);
  lowerServo.attach(44);

  BuzzerOpening();
}

void loop() {
  loraReader();
  if (charCounter > 4) {
    Serial.println(data);
    if (upper == true) {
      Serial.println("1");
      upperServoControl(data);
    }
    if (lower == true) {
      Serial.println("2");
      lowerServoControl(data);
    }
    data = 0;
    charCounter = 0;
  }
}

void upperServoControl(int rate) {
  digitalWrite(47, HIGH);
  upperServo.write(rate);
}

void lowerServoControl(int rate) {
  digitalWrite(45, HIGH);
  lowerServo.write(rate);
}

void loraReader() {
  if (Serial1.available() >= 1) {
    char loraCommand = Serial1.read();
    int a = loraCommand - '0';
    if (charCounter == 0) {
      if (loraCommand == 'u') {
        Serial.println("üst");
        upper = true;
        lower = false;
      }
      if (loraCommand == 'l') {
        Serial.println("alt");
        upper = false;
        lower = true;
      }
    }
    if (charCounter == 1) {
      data = a * 100;
    }
    if (charCounter == 2) {
      data += a * 10;
    }
    if (charCounter == 3) {
      data += a * 1;
    }
    charCounter++;
    Serial1.flush();
  }
}

void BuzzerOpening() {
  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}
