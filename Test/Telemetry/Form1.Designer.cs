﻿namespace Telemetry
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.comboBoxPorts = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxBaudRate = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAltitude = new System.Windows.Forms.Label();
            this.txtEpogee = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSat = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtVel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPressure = new System.Windows.Forms.Label();
            this.txtCh1 = new System.Windows.Forms.Label();
            this.txtG = new System.Windows.Forms.Label();
            this.txtCh2 = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chartAngle = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.txtVolt = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAlg = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartAngle)).BeginInit();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.SerialPort1_DataReceived);
            // 
            // comboBoxPorts
            // 
            this.comboBoxPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPorts.FormattingEnabled = true;
            this.comboBoxPorts.Location = new System.Drawing.Point(33, 44);
            this.comboBoxPorts.Name = "comboBoxPorts";
            this.comboBoxPorts.Size = new System.Drawing.Size(98, 21);
            this.comboBoxPorts.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(29, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ports";
            // 
            // comboBoxBaudRate
            // 
            this.comboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBaudRate.FormattingEnabled = true;
            this.comboBoxBaudRate.Items.AddRange(new object[] {
            "9600",
            "115200"});
            this.comboBoxBaudRate.Location = new System.Drawing.Point(174, 44);
            this.comboBoxBaudRate.Name = "comboBoxBaudRate";
            this.comboBoxBaudRate.Size = new System.Drawing.Size(98, 21);
            this.comboBoxBaudRate.TabIndex = 2;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(16, 71);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(546, 20);
            this.progressBar1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(170, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Baud Rate";
            // 
            // buttonConnect
            // 
            this.buttonConnect.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonConnect.Font = new System.Drawing.Font("Impact", 12F);
            this.buttonConnect.Location = new System.Drawing.Point(311, 18);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(98, 47);
            this.buttonConnect.TabIndex = 5;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDisconnect.Font = new System.Drawing.Font("Impact", 12F);
            this.buttonDisconnect.Location = new System.Drawing.Point(448, 18);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(98, 47);
            this.buttonDisconnect.TabIndex = 6;
            this.buttonDisconnect.Text = "Disconnect";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Location = new System.Drawing.Point(16, 425);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(530, 199);
            this.richTextBoxLog.TabIndex = 7;
            this.richTextBoxLog.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Impact", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(41, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "Yükseklik : ";
            // 
            // txtAltitude
            // 
            this.txtAltitude.AutoSize = true;
            this.txtAltitude.Font = new System.Drawing.Font("Impact", 30F, System.Drawing.FontStyle.Bold);
            this.txtAltitude.Location = new System.Drawing.Point(8, 155);
            this.txtAltitude.Name = "txtAltitude";
            this.txtAltitude.Size = new System.Drawing.Size(160, 48);
            this.txtAltitude.TabIndex = 9;
            this.txtAltitude.Text = "0000.00";
            // 
            // txtEpogee
            // 
            this.txtEpogee.AutoSize = true;
            this.txtEpogee.Font = new System.Drawing.Font("Impact", 30F, System.Drawing.FontStyle.Bold);
            this.txtEpogee.Location = new System.Drawing.Point(413, 155);
            this.txtEpogee.Name = "txtEpogee";
            this.txtEpogee.Size = new System.Drawing.Size(160, 48);
            this.txtEpogee.TabIndex = 11;
            this.txtEpogee.Text = "0000.00";
            this.txtEpogee.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Impact", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(443, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 26);
            this.label5.TabIndex = 10;
            this.label5.Text = "Zirve : ";
            // 
            // txtSat
            // 
            this.txtSat.AutoSize = true;
            this.txtSat.Font = new System.Drawing.Font("Impact", 30F, System.Drawing.FontStyle.Bold);
            this.txtSat.Location = new System.Drawing.Point(430, 231);
            this.txtSat.Name = "txtSat";
            this.txtSat.Size = new System.Drawing.Size(116, 48);
            this.txtSat.TabIndex = 15;
            this.txtSat.Text = "00.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Impact", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(447, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 26);
            this.label6.TabIndex = 14;
            this.label6.Text = "Sat Val : ";
            // 
            // txtVel
            // 
            this.txtVel.AutoSize = true;
            this.txtVel.Font = new System.Drawing.Font("Impact", 30F, System.Drawing.FontStyle.Bold);
            this.txtVel.Location = new System.Drawing.Point(38, 231);
            this.txtVel.Name = "txtVel";
            this.txtVel.Size = new System.Drawing.Size(116, 48);
            this.txtVel.TabIndex = 13;
            this.txtVel.Text = "00.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Impact", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(40, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 26);
            this.label8.TabIndex = 12;
            this.label8.Text = "Dikey Hız : ";
            // 
            // txtPressure
            // 
            this.txtPressure.AutoSize = true;
            this.txtPressure.Font = new System.Drawing.Font("Impact", 25F, System.Drawing.FontStyle.Bold);
            this.txtPressure.Location = new System.Drawing.Point(26, 320);
            this.txtPressure.Name = "txtPressure";
            this.txtPressure.Size = new System.Drawing.Size(139, 42);
            this.txtPressure.TabIndex = 17;
            this.txtPressure.Text = "0000.00";
            // 
            // txtCh1
            // 
            this.txtCh1.AutoSize = true;
            this.txtCh1.Font = new System.Drawing.Font("Impact", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCh1.Location = new System.Drawing.Point(56, 294);
            this.txtCh1.Name = "txtCh1";
            this.txtCh1.Size = new System.Drawing.Size(83, 26);
            this.txtCh1.TabIndex = 16;
            this.txtCh1.Text = "Basınç : ";
            // 
            // txtG
            // 
            this.txtG.AutoSize = true;
            this.txtG.Font = new System.Drawing.Font("Impact", 25F, System.Drawing.FontStyle.Bold);
            this.txtG.Location = new System.Drawing.Point(445, 320);
            this.txtG.Name = "txtG";
            this.txtG.Size = new System.Drawing.Size(82, 42);
            this.txtG.TabIndex = 19;
            this.txtG.Text = "0.00";
            // 
            // txtCh2
            // 
            this.txtCh2.AutoSize = true;
            this.txtCh2.Font = new System.Drawing.Font("Impact", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCh2.Location = new System.Drawing.Point(443, 292);
            this.txtCh2.Name = "txtCh2";
            this.txtCh2.Size = new System.Drawing.Size(101, 26);
            this.txtCh2.TabIndex = 18;
            this.txtCh2.Text = "G kuvveti : ";
            // 
            // txtTime
            // 
            this.txtTime.AutoSize = true;
            this.txtTime.Font = new System.Drawing.Font("Impact", 15F, System.Drawing.FontStyle.Bold);
            this.txtTime.Location = new System.Drawing.Point(261, 223);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(36, 25);
            this.txtTime.TabIndex = 21;
            this.txtTime.Text = "00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Impact", 10F);
            this.label10.Location = new System.Drawing.Point(242, 205);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "Sistem süresi : ";
            // 
            // txtState
            // 
            this.txtState.AutoSize = true;
            this.txtState.Font = new System.Drawing.Font("Impact", 30F, System.Drawing.FontStyle.Bold);
            this.txtState.Location = new System.Drawing.Point(186, 335);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(213, 48);
            this.txtState.TabIndex = 22;
            this.txtState.Text = "Beklemede";
            this.txtState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(12, 409);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Serial Input: ";
            // 
            // chartAngle
            // 
            chartArea1.Name = "ChartArea1";
            chartArea2.Name = "ChartArea2";
            chartArea3.Name = "ChartArea3";
            this.chartAngle.ChartAreas.Add(chartArea1);
            this.chartAngle.ChartAreas.Add(chartArea2);
            this.chartAngle.ChartAreas.Add(chartArea3);
            legend1.Name = "Legend1";
            this.chartAngle.Legends.Add(legend1);
            this.chartAngle.Location = new System.Drawing.Point(579, 12);
            this.chartAngle.Name = "chartAngle";
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            series1.Legend = "Legend1";
            series1.Name = "Raw X";
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea2";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            series2.Legend = "Legend1";
            series2.Name = "Raw Y";
            series3.BorderWidth = 3;
            series3.ChartArea = "ChartArea3";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            series3.Legend = "Legend1";
            series3.Name = "Altitude";
            this.chartAngle.Series.Add(series1);
            this.chartAngle.Series.Add(series2);
            this.chartAngle.Series.Add(series3);
            this.chartAngle.Size = new System.Drawing.Size(691, 612);
            this.chartAngle.TabIndex = 24;
            this.chartAngle.Text = "chart1";
            // 
            // txtVolt
            // 
            this.txtVolt.AutoSize = true;
            this.txtVolt.Font = new System.Drawing.Font("Impact", 15F, System.Drawing.FontStyle.Bold);
            this.txtVolt.Location = new System.Drawing.Point(261, 275);
            this.txtVolt.Name = "txtVolt";
            this.txtVolt.Size = new System.Drawing.Size(36, 25);
            this.txtVolt.TabIndex = 26;
            this.txtVolt.Text = "00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Impact", 10F);
            this.label9.Location = new System.Drawing.Point(242, 257);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 18);
            this.label9.TabIndex = 25;
            this.label9.Text = "Voltaj Değeri : ";
            // 
            // txtAlg
            // 
            this.txtAlg.AutoSize = true;
            this.txtAlg.Font = new System.Drawing.Font("Impact", 15F, System.Drawing.FontStyle.Bold);
            this.txtAlg.Location = new System.Drawing.Point(221, 155);
            this.txtAlg.Name = "txtAlg";
            this.txtAlg.Size = new System.Drawing.Size(36, 25);
            this.txtAlg.TabIndex = 28;
            this.txtAlg.Text = "00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Impact", 10F);
            this.label11.Location = new System.Drawing.Point(223, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(131, 18);
            this.label11.TabIndex = 27;
            this.label11.Text = "Kullanılan Algoritma : ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 636);
            this.Controls.Add(this.txtAlg);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtVolt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chartAngle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtState);
            this.Controls.Add(this.txtTime);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtG);
            this.Controls.Add(this.txtCh2);
            this.Controls.Add(this.txtPressure);
            this.Controls.Add(this.txtCh1);
            this.Controls.Add(this.txtSat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtVel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtEpogee);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAltitude);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.buttonDisconnect);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.comboBoxBaudRate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxPorts);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.chartAngle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ComboBox comboBoxPorts;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxBaudRate;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txtAltitude;
        private System.Windows.Forms.Label txtEpogee;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label txtSat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label txtVel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label txtPressure;
        private System.Windows.Forms.Label txtCh1;
        private System.Windows.Forms.Label txtG;
        private System.Windows.Forms.Label txtCh2;
        private System.Windows.Forms.Label txtTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label txtState;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAngle;
        private System.Windows.Forms.Label txtVolt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label txtAlg;
        private System.Windows.Forms.Label label11;
    }
}

