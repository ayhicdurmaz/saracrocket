﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Threading;


namespace Telemetry
{
    public partial class Form1 : Form
    {
        public delegate void AddDataDelegate(String myString);
        public AddDataDelegate myDelegate;
        string strCurrentLine = "";
        string[] datas;
        long maksm = 50;
        long minm = 0;

        public Form1()
        {
            InitializeComponent();
            getPortsNames();
            this.myDelegate = new AddDataDelegate(addDataMethod);
        }

        void getPortsNames()
        {
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                comboBoxPorts.Items.Add(port);
                comboBoxPorts.SelectedIndex = 0;
            }
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxPorts.Text == string.Empty || comboBoxBaudRate.Text == string.Empty)
                {
                    MessageBox.Show("Check Settings", "Unexpected Exception", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    serialPort1.PortName = comboBoxPorts.Text;
                    serialPort1.BaudRate = Int32.Parse(comboBoxBaudRate.Text);
                    serialPort1.DataBits = 8;
                    serialPort1.Open();
                    serialPort1.DataReceived += SerialPort1_DataReceived;
                    if (serialPort1.IsOpen)
                    {
                        progressBar1.Value = 100;
                        buttonConnect.Enabled = false;
                        buttonDisconnect.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex, "Unexpected Exception", MessageBoxButtons.OK, MessageBoxIcon.Error); ;
            }
        }

        private void SerialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                try
                {
                    SerialPort sp = (SerialPort)sender;
                    strCurrentLine = sp.ReadLine();
                    serialPort1.DiscardInBuffer();
                    sp.DiscardInBuffer();
                    richTextBoxLog.Invoke(this.myDelegate, new Object[] { strCurrentLine });
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex, "Unexcepted Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        public void addDataMethod(String myString)
        {
            try
            {
                if (myString != null)
                {
                    richTextBoxLog.Text = myString;
                    datas = myString.Split('?');
                    dataControll(datas);
                    if (datas[0] == "A")
                    {
                        txtAlg.Text = "Anakart";

                        if (datas[13] == "0")
                        {
                            txtState.Text = "Bağlandı";
                        }
                        else if(datas[13] == "1")
                        {
                            txtState.Text = "Havalandı";
                        }
                        else if(datas[13] == "2" )
                        {
                            txtState.Text = "Zirvede";
                        }
                        else if(datas[13] == "3" )
                        {
                            txtState.Text = "Zirvede";
                        }
                        else
                        {                            
                            txtEpogee.Text = "0";
                        }

                        txtTime.Text     = datas[1];
                        txtVolt.Text     = datas[2];
                        txtAltitude.Text = datas[3];
                        txtEpogee.Text   = datas[4];
                        txtVel.Text      = datas[5];
                        txtPressure.Text = datas[6];
                        txtG.Text        = datas[7];
                        txtSat.Text      = datas[10];
                        

                        ChartDesign(datas[11], datas[12], datas[3]);

                    }
                    if (datas[0] == "Y")
                    {
                        txtAlg.Text = "YedekKart";

                        if (datas[13] == "0")
                        {
                            txtState.Text = "Bağlandı";
                        }
                        else if (datas[13] == "1")
                        {
                            txtState.Text = "Havalandı";
                        }
                        else if (datas[13] == "2")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else if (datas[13] == "3")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else
                        {
                            txtEpogee.Text = "0";
                        }

                        txtTime.Text     = datas[1];
                        txtVolt.Text     = datas[2];
                        txtAltitude.Text = datas[3];
                        txtEpogee.Text   = datas[4];
                        txtVel.Text      = datas[5];
                        txtPressure.Text = datas[6];
                        txtG.Text        = datas[7];
                        txtSat.Text      = datas[10];

                        ChartDesign(datas[11], datas[12], datas[3]);
                    }
                    if (datas[0] == "TA")
                    {
                        txtAlg.Text = "TESTAnakart";

                        if (datas[10] == "0")
                        {
                            txtState.Text = "Bağlandı";
                        }
                        else if (datas[10] == "1")
                        {
                            txtState.Text = "Havalandı";
                        }
                        else if (datas[10] == "2")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else if (datas[10] == "3")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else
                        {
                            txtEpogee.Text = "0";
                        }

                        txtTime.Text = datas[1];
                        txtVolt.Text = "-";
                        txtAltitude.Text = datas[2];
                        txtEpogee.Text = datas[3];
                        txtVel.Text = datas[4];
                        txtPressure.Text = datas[6];
                        txtG.Text = datas[7];
                        txtSat.Text = "-";


                        ChartDesign(datas[8], datas[9], datas[2]);

                    }
                    if (datas[0] == "TY")
                    {
                        txtAlg.Text = "TESTYedekKart";

                        if (datas[10] == "0")
                        {
                            txtState.Text = "Bağlandı";
                        }
                        else if (datas[10] == "1")
                        {
                            txtState.Text = "Havalandı";
                        }
                        else if (datas[10] == "2")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else if (datas[10] == "3")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else
                        {
                            txtEpogee.Text = "0";
                        }

                        txtTime.Text = datas[1];
                        txtVolt.Text = "-";
                        txtAltitude.Text = datas[2];
                        txtEpogee.Text = datas[3];
                        txtVel.Text = datas[4];
                        txtPressure.Text = datas[6];
                        txtG.Text = datas[7];
                        txtSat.Text = "-";


                        ChartDesign(datas[8], datas[9], datas[2]);
                    }
                    if (datas[0] == "TG")
                    {
                        txtAlg.Text = "Telemetri testi";

                        if (datas[14] == "0")
                        {
                            txtState.Text = "Bağlandı";
                        }
                        else if (datas[14] == "1")
                        {
                            txtState.Text = "Havalandı";
                        }
                        else if (datas[14] == "2")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else if (datas[14] == "3")
                        {
                            txtState.Text = "Zirvede";
                        }
                        else
                        {
                            txtEpogee.Text = "0";
                        }

                        txtTime.Text     = datas[1];
                        txtVolt.Text     = datas[2];
                        txtAltitude.Text = datas[3];
                        txtEpogee.Text   = datas[4];
                        txtVel.Text      = datas[5];
                        txtPressure.Text = datas[6];
                        txtG.Text        = datas[7];
                        txtSat.Text      = datas[10];


                       // ChartDesign(datas[2], datas[5]);

                    }
                    if (datas[0] == "T")
                    {
                        txtState.Text = "Kalman Test";
                       // ChartDesign(datas[2], datas[5]);
                    }

                    logger(myString);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex, "AddData", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void ChartDesign(string x1, string x2, string x3)
        {
            //ChartX
            this.chartAngle.ChartAreas[0].AxisX.Minimum = minm;
            this.chartAngle.ChartAreas[0].AxisX.Maximum = maksm;

            this.chartAngle.ChartAreas[0].AxisY.Minimum = -180;
            this.chartAngle.ChartAreas[0].AxisY.Maximum = 180;

            this.chartAngle.ChartAreas[0].AxisX.ScaleView.Zoom(minm, maksm);

            this.chartAngle.Series[0].Points.AddXY((minm + maksm) / 2, x1);
            //ChartY
            this.chartAngle.ChartAreas[1].AxisX.Minimum = minm;
            this.chartAngle.ChartAreas[1].AxisX.Maximum = maksm;

            this.chartAngle.ChartAreas[1].AxisY.Minimum = -180;
            this.chartAngle.ChartAreas[1].AxisY.Maximum = 180;

            this.chartAngle.ChartAreas[1].AxisX.ScaleView.Zoom(minm, maksm);

            this.chartAngle.Series[1].Points.AddXY((minm + maksm) / 2, x2);
            //Altitude
            this.chartAngle.ChartAreas[2].AxisX.Minimum = minm;
            this.chartAngle.ChartAreas[2].AxisX.Maximum = maksm;
            
            this.chartAngle.ChartAreas[2].AxisY.Minimum = -10;
            this.chartAngle.ChartAreas[2].AxisY.Maximum = float.Parse(x3) + 20;
            
            this.chartAngle.ChartAreas[2].AxisX.ScaleView.Zoom(minm, maksm);

            this.chartAngle.Series[2].Points.AddXY((minm + maksm) / 2, x3);

            this.chartAngle.Update();

            minm++;
            maksm++;
        }

        public void dataControll(string[] _datas)
        {
            int i = 0;
            foreach (string value in _datas)
            {
                if (value == string.Empty)
                {
                    _datas[i] = "0";
                    i++;
                }
            }
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                try
                {

                    serialPort1.Close();
                    serialPort1.DataReceived -= SerialPort1_DataReceived;
                    if (!serialPort1.IsOpen)
                    {
                        progressBar1.Value = 0;
                        buttonConnect.Enabled = true;
                        buttonDisconnect.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex, "disconnect", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        void logger(string strLog)
        {
            try
            {
                string path = @"c:\\Logs\log.txt";
                using (StreamWriter file = new StreamWriter(path, true))
                {
                    file.Write( strLog + "\n");
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex, "Log", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
     
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                try
                {
                    serialPort1.Close();
                    serialPort1.DataReceived -= SerialPort1_DataReceived;
                    if (!serialPort1.IsOpen)
                    {
                        progressBar1.Value = 0;
                        buttonConnect.Enabled = true;
                        buttonDisconnect.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex, "disconnect", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}

