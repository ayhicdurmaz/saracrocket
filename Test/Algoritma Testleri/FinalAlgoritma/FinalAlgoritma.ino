//Defining Libraries
#include "SoftwareSerial.h"
#include "MPU6050_tockn.h"
#include "Seeed_BME280.h"
#include "SFE_BMP180.h"
#include "TinyGPS++.h"
#include "Kalman.h"
#include "Servo.h"
#include "Wire.h"
#include "SPI.h"
#include "SD.h"

//States
#define LoraConnection //comment if you use lora
//#define Bme280 //comment if you use bmp180
bool Anakart = true;

//Constanst
#define MZ A14
#define MO A13
#define Red 31
#define Blue 30
#define Green 32
#define Buzzer 33
#define FallDam 5
#define DamValue 0
#define servoPin 8
#define voltmeter A15
#define BaudRate 115200



//BMP180 variables
#ifdef Bme280
BME280 bme;
#else
SFE_BMP180 bmp;
#endif
float baseline, pressure, alt, apogeeAlt, bottomAlt, verticalVelocity, curAlt;

//MPU6050 variables
MPU6050 mpu(Wire);
double gyroXangle, gyroYangle; // Angle calculate using the gyro only
double kalAngleX, kalAngleY; // Calculated angle using a Kalman filter
float x_offset, y_offset;
float g;

//GPS
float longtitude;
float latitude;
String strlongtitude = "0.0000000";
String strlatitude = "0.0000000";
String curLongtitude;
String curLatitude;
byte satVal;
TinyGPSPlus gps;


// Create the Kalman instances
Kalman kalmanX;
Kalman kalmanY;

//Servo variable
Servo servo;

//Sistem variables
float volt;
bool apogee = false;
String data = "";
uint32_t dtime = 0;
uint32_t timer = 0;
float loraTimer = 0;
char loraCommand;
int state = 0;
int fallingCheck = 0;

void setup() {

  delay(100);  //Sensörlere güç gitmesi ve i2c için beklenmesi için bekleme

  Serial.begin(BaudRate);

  Serial1.begin(BaudRate);  //lora haberleşmesi başlatma

  Serial2.begin(BaudRate);  //GPS başlatılması

  pinMode(voltmeter, INPUT);
  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);
  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  Wire.begin(); // I2C

  mpu.begin();  //Accellometre ve Gyroskobun açılması

  servo.attach(servoPin);

  servo.write(165);
#ifndef Bme280
  if (bmp.begin())
    Serial.println("BMP180 init success");
  else
  {
    // Oops, something went wrong, this is usually a connection problem,
    // see the comments at the top of this sketch for the proper connections.

    Serial.println("BMP180 init fail (disconnected?)\n\n");
    while (1); // Pause forever.
  }
#else
  if (!bme.init()) {
        Serial.println("Device error!");
    }
#endif

  /*if (!bme280.init()) {  // Sensor bağlanıyor
    Serial.println("Device error!");
    while (1);
    }*/

  calcOffset(); //Offset values for mpu

  pinMode(Buzzer, OUTPUT);
  pinMode(Red, OUTPUT);
  pinMode(Green, OUTPUT);
  pinMode(Blue, OUTPUT);

  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}

void loop() {
  mpu.update();  //MPU sensor update
  readGPS();
  readVolt();
  
#ifdef Bme280
  pressure = bme.getPressure(); //hPa türüne çevrildi
  alt = bme.calcAltitude(pressure, baseline);
#else  
  pressure = getPressure(); //hPa türüne çevrildi
  alt = bmp.altitude(pressure, baseline);
#endif

  if (state == 0) { //Roket uçmaya başladı
    if (alt > DamValue) {
      state = 1;
    }
  }

  if (alt > apogeeAlt) {
    apogeeAlt = alt;
  }

  //Açı ölçme
  kalmanFiltering();

  // G kuvveti hesaplama
  g = calcG(mpu.getAccX(), mpu.getAccY(), mpu.getAccZ());

  if (apogee == false) {
    checkForApogee();
    calcV(alt, curAlt);
    curAlt = alt;
  }
  if (apogee == true) {
    if (state < 2) {
      state = 2;
    }
    if (alt < 600) {
      state = 3;
    }
  }

  if (millis() - loraTimer > 100) {
    sendData();
    loraTimer = millis();
  }
}

void readVolt(){
  volt = float(analogRead(voltmeter)) * 10 / 1023;
}

void calcOffset() {
  for (int i = 0; i < 1000; i++) { //Offset değerlerini ayarlama
    mpu.update();
    kalmanFiltering();
    x_offset += kalAngleX;
    y_offset += kalAngleY;
  }
  #ifdef Bme280
  baseline = bme.getPressure();
  #else
  baseline = getPressure();  //hPa türünde
  #endif
  x_offset = x_offset / 3000;
  y_offset = y_offset / 3000;
}

void readGPS() {
  while (Serial2.available() > 1)
  {
    gps.encode(Serial2.read());
    satVal = gps.satellites.value();
    if (gps.location.isUpdated())
    {
      latitude = gps.location.lat();
      longtitude = gps.location.lng();
      strlatitude = String(latitude, 6);
      strlongtitude = String(longtitude, 6);
    }
  }
}

void calcV(float x1, float x2) {
  double dt = (double)(millis() - dtime) / 100; // Calculate delta time
  dtime = millis();
  verticalVelocity = (x1 - x2) / dt;
}

float calcG(float accX, float accY, float accZ) {
  return (sqrt(pow(accX, 2) + pow(accY, 2) + pow(accZ, 2))); //G kuvvetinin hesaplanması
}

void setRGB(int red, int green, int blue) {
  red = 255 - red;
  green = 255 - green;
  blue = 255 - blue;
  analogWrite(Red, red);
  analogWrite(Green, green);
  analogWrite(Blue, blue);
}

void kalmanFiltering() {
  float accX = mpu.getRawAccX();
  float accY = mpu.getRawAccY();
  float accZ = mpu.getRawAccZ();
  float tempRaw = mpu.getRawTemp();
  float gyroX = mpu.getRawGyroX();
  float gyroY = mpu.getRawGyroY();
  float gyroZ = mpu.getRawGyroZ();

  double dt = (double)(micros() - timer) / 1000000; // Calculate delta time
  timer = micros();

  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;

  double gyroXrate = gyroX / 131.0; // Convert to deg/s
  double gyroYrate = gyroY / 131.0; // Convert to deg/s

  // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
  if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
    kalmanX.setAngle(roll);
    kalAngleX = roll;
    gyroXangle = roll;
  } else
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter

  if (abs(kalAngleX) > 90)
    gyroYrate = -gyroYrate; // Invert rate, so it fits the restriced accelerometer reading
  kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);

  gyroXangle += gyroXrate * dt; // Calculate gyro angle without any filter
  gyroYangle += gyroYrate * dt;

  // Reset the gyro angle when it has drifted too much
  if (gyroXangle < -180 || gyroXangle > 180)
    gyroXangle = kalAngleX;
  if (gyroYangle < -180 || gyroYangle > 180)
    gyroYangle = kalAngleY;
}

void checkForApogee() {
  if (alt > DamValue) {
    if (abs(kalAngleX - x_offset) >= 65) {
      apogee = true;
      servo.write(0);
    }

    else if (abs(kalAngleY - y_offset) >= 65) {
      apogee = true;
      servo.write(0);
    }
    if (alt > apogeeAlt) {
      apogeeAlt = alt;
      bottomAlt = alt - 0.5;
      if (fallingCheck > 0) {
        fallingCheck--;
      }
    }
    else if (bottomAlt > alt) {
      bottomAlt = alt;
      fallingCheck++;
    }
    if (fallingCheck > FallDam) {
      apogee = true;
      servo.write(85);
    }
  }
}

void setData() {
  data =  "Y";                                 // 0. data type
  data += "?";  data += int(millis() / 1000);   // 1. System time
  data += "?";  data += volt;                  // 2. System Volt
  data += "?";  data += alt;                   // 3. latest alt
  data += "?";  data += apogeeAlt;             // 4. apogee
  data += "?";  data += verticalVelocity;      // 5. vertical velocity
  data += "?";  data += pressure / 100;              // 6. pressure (hPa)
  data += "?";  data += g;                     // 7. g power
  data += "?";  data += strlatitude;           // 8. Latitude
  data += "?";  data += strlongtitude;         // 9. Longtitude
  data += "?";  data += satVal;                // 10. Satellite Value
  data += "?";  data += kalAngleX;             // 11. kalman X angle
  data += "?";  data += kalAngleY;             // 12. kalman Y angle
  data += "?";  data += state;                 // 13. state
  data += "?";  data += "*";                   // 14. finish command
}

void sendData() {
  setData();
#ifdef LoraConnection
  Serial1.println(data);
  Serial1.flush();   // buffer temizlemek
#else
  Serial.println(data);
  Serial.flush();   // buffer temizlemek
#endif
}


#ifndef Bme280
double getPressure()
{
  char status;
  double T, P, p0, a;

  // You must first get a temperature measurement to perform a pressure reading.

  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.

  status = bmp.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:

    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Use '&T' to provide the address of T to the function.
    // Function returns 1 if successful, 0 if failure.

    status = bmp.getTemperature(T);
    if (status != 0)
    {
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = bmp.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Use '&P' to provide the address of P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = bmp.getPressure(P, T);
        if (status != 0)
        {
          return (P);
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");
}
#endif
