//Defining Libraries
#include "MPU6050_tockn.h"
#include "Kalman.h"
#include "Wire.h"

//States
#define LoraConnection //comment if you use lora

//Constanst
#define MZ A14
#define MO A13
#define Buzzer 33
#define BaudRate 115200

//MPU6050 variables
MPU6050 mpu(Wire);
float g;
float x_offset, y_offset;
double kalAngleX, kalAngleY; // Calculated angle using a Kalman filter
double AccX, AccY; // Other Filter
double gyroXangle, gyroYangle; // Angle calculate using the gyro only

// Create the Kalman instances
Kalman kalmanX;
Kalman kalmanY;

//Sistem variables
uint32_t timer   = 0;
float loraTimer  = 0;
String data      = "";

void setup() {

  delay(100);  //Sensörlere güç gitmesi ve i2c için beklenmesi için bekleme

  Serial.begin(BaudRate);

  Serial1.begin(BaudRate);  //lora haberleşmesi başlatma

  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);
  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  Wire.begin(); // I2C

  mpu.begin();  //Accellometre ve Gyroskobun açılması

  pinMode(Buzzer, OUTPUT);

  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}

void loop() {
  mpu.update();  //MPU sensor update

  AccX = mpu.getAccAngleX();
  AccY = mpu.getAccAngleY();
  
  //Açı ölçme
  kalmanFiltering();

  if (millis() - loraTimer > 100) {
    sendData();
    loraTimer = millis();
  }
}

void kalmanFiltering() {
  float accX = mpu.getRawAccX();
  float accY = mpu.getRawAccY();
  float accZ = mpu.getRawAccZ();
  float tempRaw = mpu.getRawTemp();
  float gyroX = mpu.getRawGyroX();
  float gyroY = mpu.getRawGyroY();
  float gyroZ = mpu.getRawGyroZ();

  double dt = (double)(micros() - timer) / 1000000; // Calculate delta time
  timer = micros();

  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;

  double gyroXrate = gyroX / 131.0; // Convert to deg/s
  double gyroYrate = gyroY / 131.0; // Convert to deg/s

  // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
  if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
    kalmanX.setAngle(roll);
    kalAngleX = roll;
    gyroXangle = roll;
  } else
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter

  if (abs(kalAngleX) > 90)
    gyroYrate = -gyroYrate; // Invert rate, so it fits the restriced accelerometer reading
  kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);

  gyroXangle += gyroXrate * dt; // Calculate gyro angle without any filter
  gyroYangle += gyroYrate * dt;

  // Reset the gyro angle when it has drifted too much
  if (gyroXangle < -180 || gyroXangle > 180)
    gyroXangle = kalAngleX;
  if (gyroYangle < -180 || gyroYangle > 180)
    gyroYangle = kalAngleY;
}


void setData() {
  data =  "T";                                 // 0. data type
  data += "?";  data += int(millis() / 100);   // 1. System time
  data += "?";  data += gyroXangle;   // 2. System time
  data += "?";  data += AccX;   // 3. System time
  data += "?";  data += kalAngleX;   // 4. System time
  data += "?";  data += gyroYangle;   // 5. System time
  data += "?";  data += AccY;   // 6. System time
  data += "?";  data += kalAngleY;   // 7. System time
  data += "?";  data += "*";   // 7. System time
}

void sendData() {
  setData();
#ifdef LoraConnection
  Serial1.println(data);
  Serial1.flush();   // buffer temizlemek
#else
  Serial.println(data);
  Serial.flush();   // buffer temizlemek
#endif
}
