#include <MPU6050_tockn.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include <Timer.h>

MPU6050 mpu(Wire);
float accX, accY, accZ;
float angleX, angleY, angleZ, gyroX, gyroY;
float x_offset, y_offset, z_offset;
float g;

#define TX 5
#define RX 4
#define BaudRate 115200
SoftwareSerial lora(RX, TX); //RX, TX
Timer send_data;
String data = "";
const int buzz = 7;

void setup() {
  Serial.begin(9600);
  lora.begin(BaudRate);
  Wire.begin();
  mpu.begin();
  for (int i = 0; i < 3000; i++) { //Offset değerlerini ayarlama
    mpu.update();
    x_offset += mpu.getAngleX();
    y_offset += mpu.getAngleY();
  }
  x_offset = x_offset / 3000;
  y_offset = y_offset / 3000;

  send_data.every(100, dataSend);
  pinMode(buzz, OUTPUT);
  buzzer(1000);
}

void loop() {
  mpu.update();
  angleX = mpu.getAngleX() - x_offset;
  angleY = mpu.getAngleY() - y_offset;
  accX = mpu.getAccX();
  accY = mpu.getAccY();
  accZ = mpu.getAccZ();
  gyroX = mpu.getGyroX();
  gyroY = mpu.getGyroY();
  
  g = sqrt(pow(accX, 2) + pow(accY, 2) + pow(accZ, 2));
  send_data.update();
}

void setData() {
  data =  "T";                         
  data += "?";  data += accX;  
  data += "?";  data += accY;     
  data += "?";  data += accZ;           
  data += "?";  data += gyroX;    
  data += "?";  data += gyroY;          
  data += "?";  data += angleX;     
  data += "?";  data += angleY;     
  data += "?";  data += "*";           
}
void buzzer(int ms) {
  tone(buzz, 1000);
  delay(ms);
  noTone(buzz);
}
void dataSend() {
  setData();
  lora.println(data);
  lora.flush();   // buffer temizlemek
  data = "";
}
