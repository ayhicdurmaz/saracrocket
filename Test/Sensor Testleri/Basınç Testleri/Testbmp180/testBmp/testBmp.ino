#include <BMP180.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>

BMP180 pressure(BMP180_ULTRAHIGHRES);

double baseline;

void setup()
{
  Serial.begin(9600);
  if(!pressure.begin()){
    Serial.println("Sensor failed, or not present");
    while(1);
  }
  Serial.println("sensor initialized.");
  
  if (!SD.begin(4)) {
    Serial.println("Card failed, or not present");
    while(1);
  }
  Serial.println("card initialized.");

  baseline = pressure.getPressure();
}

void loop()
{
  unsigned long StartTime = millis();
  String data = "";
  double alt, pres;

  pres = pressure.getPressure();

  alt = 44330.0*(1-pow(pres/baseline,1/5.255));

  unsigned long CurrentTime = millis();
  unsigned long ElapsedTime = CurrentTime - StartTime;

  unsigned long Time = ElapsedTime + Time;

  data = ElapsedTime; data += ","; data += alt;
  Serial.println(data);
  
  //dataLogger(data);
  

}
/*
void dataLogger(String data){
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  if(dataFile){
    dataFile.println(data);
    dataFile.close();
    Serial.println(data);
  }
  else {
    Serial.println("error opening datalog.txt");  
  }
}*/
