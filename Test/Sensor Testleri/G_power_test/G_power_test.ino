#include <MPU6050_tockn.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#define BaudRate 115200
#define TX 5
#define RX 4
#define BaudRate 115200
SoftwareSerial lora(RX, TX); //RX, TX

MPU6050 mpu(Wire);
float accX, accY, accZ;
float g;

String data = "";

float timer = 0;

void setup() {
  Serial.begin(BaudRate);
  lora.begin(BaudRate);
  Wire.begin();
  mpu.begin();
}

void loop() {
  mpu.update();

  accX = mpu.getAccX();
  accY = mpu.getAccY();
  accZ = mpu.getAccZ();

  g = sqrt(pow(accX, 2) + pow(accY, 2) + pow(accZ, 2));
  if (millis() - timer > 100) {
    data =  "T";
    data += "?";  data += accX;
    data += "?";  data += accY;
    data += "?";  data += accZ;
    data += "?";  data += g;
    data += "?";  data += "*";
    lora.println(data);
    lora.flush();   // buffer temizlemek
    data = "";
    timer = millis();
  }
}
