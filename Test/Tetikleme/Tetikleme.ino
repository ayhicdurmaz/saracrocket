#include "SoftwareSerial.h"

#define MZ 5
#define MO 6
#define TX 7
#define RX 8
#define BaudRate 115200
#define Button 4

//Telemetry variables
SoftwareSerial lora(RX, TX); //RX, TX

//System variables
int counter = 1;
bool low = false;
int timer = 0;

void setup() {
  Serial.begin(BaudRate);  //Serial haberleşmeyi başlatma

  lora.begin(BaudRate);  //lora haberleşmesi başlatma

  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);
  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  pinMode(Button, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);

}

void loop() {
  if (low) {
    if (digitalRead(Button) == 1) {
      counter++;
      low = false;
    }
  }
  if (digitalRead(Button) == 0) {
    low = true;
  }
  if (millis() - timer > 1000) {
    if (counter % 2 == 1) {
      lora.write('1');
      lora.flush();
      digitalWrite(LED_BUILTIN, HIGH);
    } else {
      lora.write('2');
      lora.flush();
      digitalWrite(LED_BUILTIN, LOW);
    }
    timer = millis();
  }
}
