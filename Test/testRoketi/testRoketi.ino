//Defining Libraries
#include "SoftwareSerial.h"
#include "Kalman.h"
#include "SD.h"
#include "SPI.h"
#include "Wire.h"
#include "Seeed_BME280.h"
#include "MPU6050_tockn.h"
#include "Servo.h"

//States
#define LoraConnection //comment if you use lora
bool Anakart = false;

//Constanst
#define MZ 11
#define MO 12
#define TX 5
#define RX 4
#define BaudRate 115200
#define Buzzer 10
#define servoPin 7
#define DamValue 0
#define FallDam 5

//Servo variable
Servo servo;

//Moving Avarage
//MovingAverage <float> avgAlt(20);

//Telemetry variables
SoftwareSerial lora(RX, TX); //RX, TX

//BME280 variables
BME280 bme280;
float baseline, pressure, alt, epogeeAlt, bottomAlt, verticalVelocity, verticalAccel, curVV, curAlt;

//MPU6050 variables
MPU6050 mpu(Wire);
double gyroXangle, gyroYangle; // Angle calculate using the gyro only
double kalAngleX, kalAngleY; // Calculated angle using a Kalman filter
float x_offset, y_offset;
float g;

// Create the Kalman instances
Kalman kalmanX;
Kalman kalmanY;

//Sistem variables
bool apogee = false;
String data = "";
uint32_t dtime = 0;
uint32_t timer = 0;
float loraTimer = 0;
char loraCommand;
int state = 0;
int fallingCheck = 0;

void setup() {

  delay(100);  //Sensörlere güç gitmesi ve i2c için beklenmesi için bekleme

  Serial.begin(BaudRate);  //Serial haberleşmeyi başlatma

  lora.begin(BaudRate);  //lora haberleşmesi başlatma

  pinMode(MZ, OUTPUT);
  pinMode(MO, OUTPUT);
  digitalWrite(MZ, LOW);
  digitalWrite(MO, LOW);

  Wire.begin(); // I2C

  mpu.begin();  //Accellometre ve Gyroskobun açılması

  servo.attach(servoPin);

  servo.write(125);

  if (!bme280.init()) {  // Sensor bağlanıyor
    Serial.println("Device error!");
    while (1);
  }

  calcOffset(); //Offset values for mpu

  pinMode(Buzzer, OUTPUT);

  tone(Buzzer, 1000);
  delay(100);
  tone(Buzzer, 1200);
  delay(100);
  tone(Buzzer, 1400);
  delay(300);
  tone(Buzzer, 1600);
  delay(100);
  noTone(Buzzer);
}

void loop() {
  mpu.update();  //MPU sensor update
  readLora();

  pressure = bme280.getPressure(); //hPa türüne çevrildi
  alt = bme280.calcAltitude(pressure, baseline);
  //alt = avgAlt.CalculateMovingAverage(alt);

  if (state == 0) { //Roket uçmaya başladı
    if (alt > DamValue) {
      state = 1;
    }
  }

  if (alt > epogeeAlt) {
    epogeeAlt = alt;
    bottomAlt = alt - 0.5;
  }

  //Açı ölçme
  kalmanFiltering();

  // G kuvveti hesaplama
  g = calcG(mpu.getAccX(), mpu.getAccY(), mpu.getAccZ());

  if (apogee == false) {
    checkForApogee();
    calcV(alt, curAlt);
    curAlt = alt;
  }
  if (apogee == true) {
    if (state < 2) {
      state = 2;
    }
    if (alt < 600) {
      state = 3;
    }
    curAlt = alt;
  }

  if (millis() - loraTimer > 100) {
    sendData();
    loraTimer = millis();
  }
}

void readLora() {
  while (lora.available() > 1) {
    loraCommand = lora.read();
    Serial.println(loraCommand);
    lora.flush();
  }
  if (loraCommand == '1') {
    if (!Anakart) {
      tone(Buzzer, 900);
      delay(250);
      tone(Buzzer, 1000);
      delay(200);
      noTone(Buzzer);
    }
    Anakart = true;
  }
  else if (loraCommand == '2') {
    if (Anakart) {
      tone(Buzzer, 1000);
      delay(250);
      tone(Buzzer, 900);
      delay(200);
      noTone(Buzzer);
    }
    Anakart = false;
  }
}

void calcV(float x1, float x2) {
  double dt = (double)(millis() - dtime) / 100; // Calculate delta time
  dtime = millis();
  verticalVelocity = (x1 - x2) / dt;
}

float calcG(float accX, float accY, float accZ) {
  return (sqrt(pow(accX, 2) + pow(accY, 2) + pow(accZ, 2))); //G kuvvetinin hesaplanması
}

void calcOffset() {
  for (int i = 0; i < 1000; i++) { //Offset değerlerini ayarlama
    mpu.update();
    kalmanFiltering();
    x_offset += kalAngleX;
    y_offset += kalAngleY;
  }
  baseline = bme280.getPressure();  //hPa türünde
  x_offset = x_offset / 3000;
  y_offset = y_offset / 3000;
}

void kalmanFiltering() {
  float accX = mpu.getRawAccX();
  float accY = mpu.getRawAccY();
  float accZ = mpu.getRawAccZ();
  float tempRaw = mpu.getRawTemp();
  float gyroX = mpu.getRawGyroX();
  float gyroY = mpu.getRawGyroY();
  float gyroZ = mpu.getRawGyroZ();

  double dt = (double)(micros() - timer) / 1000000; // Calculate delta time
  timer = micros();

  double roll  = atan2(accY, accZ) * RAD_TO_DEG;
  double pitch = atan(-accX / sqrt(accY * accY + accZ * accZ)) * RAD_TO_DEG;

  double gyroXrate = gyroX / 131.0; // Convert to deg/s
  double gyroYrate = gyroY / 131.0; // Convert to deg/s

  // This fixes the transition problem when the accelerometer angle jumps between -180 and 180 degrees
  if ((roll < -90 && kalAngleX > 90) || (roll > 90 && kalAngleX < -90)) {
    kalmanX.setAngle(roll);
    kalAngleX = roll;
    gyroXangle = roll;
  } else
    kalAngleX = kalmanX.getAngle(roll, gyroXrate, dt); // Calculate the angle using a Kalman filter

  if (abs(kalAngleX) > 90)
    gyroYrate = -gyroYrate; // Invert rate, so it fits the restriced accelerometer reading
  kalAngleY = kalmanY.getAngle(pitch, gyroYrate, dt);

  gyroXangle += gyroXrate * dt; // Calculate gyro angle without any filter
  gyroYangle += gyroYrate * dt;

  // Reset the gyro angle when it has drifted too much
  if (gyroXangle < -180 || gyroXangle > 180)
    gyroXangle = kalAngleX;
  if (gyroYangle < -180 || gyroYangle > 180)
    gyroYangle = kalAngleY;
}

void checkForApogee() {
  if (Anakart) {
    if (g < 0.2) {
      apogee = true;
      servo.write(0);
      tone(Buzzer, 1000);
      delay(250);
      noTone(Buzzer);
    }
  }
  else if (!Anakart) {
    if (abs(kalAngleX - x_offset) >= 65) {
      apogee = true;
      servo.write(0);
    }

    else if (abs(kalAngleY - y_offset) >= 65) {
      apogee = true;
      servo.write(0);
    }
    if (bottomAlt > alt) {
      bottomAlt = alt;
      fallingCheck++;
    } else if (alt > bottomAlt) {
      if (fallingCheck > 0) {
        fallingCheck = 0;
      }
    }
    if (fallingCheck > FallDam) {
      apogee = true;
      servo.write(85);
      tone(Buzzer, 1000);
      delay(250);
      noTone(Buzzer);
    }
  }
}

void setData() {
  if (Anakart) {
    data = "TA";
  } else {
    data = "TY";
  }
  data += "?";  data += int(millis() / 1000);   // 1. System time
  data += "?";  data += alt;                   // 2. latest deep alt
  data += "?";  data += epogeeAlt;             // 3. latest apogee alt
  data += "?";  data += verticalVelocity;      // 4. vertical velocity
  data += "?";  data += verticalAccel;         // 5. vertical accel
  data += "?";  data += pressure;              // 6. pressure (hPa)
  data += "?";  data += g;                     // 7. g power
  data += "?";  data += kalAngleX;             // 8. kalman X angle
  data += "?";  data += kalAngleY;             // 9. kalman Y angle
  data += "?";  data += state;                 // 10. state
  data += "?";  data += "*";                   // 11. finish command
}

void sendData() {
  setData();
#ifdef LoraConnection
  lora.println(data);
  lora.flush();   // buffer temizlemek
#else
  Serial.println(data);
  Serial.flush();   // buffer temizlemek
#endif
}
